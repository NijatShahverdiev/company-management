package dev.companymanagement.model.request;

import jakarta.validation.constraints.NotBlank;
import lombok.AllArgsConstructor;
import lombok.Getter;


@Getter
@AllArgsConstructor

public class CompanyRequestDto {
    @NotBlank
    private String name;
    private Long parentCompanyId;
}
