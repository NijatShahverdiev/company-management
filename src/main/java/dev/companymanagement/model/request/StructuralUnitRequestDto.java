package dev.companymanagement.model.request;

import jakarta.validation.constraints.NotBlank;
import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class StructuralUnitRequestDto {
    @NotBlank(message = "Structure name must be defined")
    private String name;
    private Long parentId;
    private Long companyId;
}
