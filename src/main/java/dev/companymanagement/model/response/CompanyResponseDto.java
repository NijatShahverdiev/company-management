package dev.companymanagement.model.response;

import lombok.*;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
public class CompanyResponseDto {

    public CompanyResponseDto(Long id, String name) {
        this.id = id;
        this.name = name;
    }

    private Long id;
    private String name;
    private Long parentId;
    private Boolean isCompany;
    private List<CompanyResponseDto> children = new ArrayList<>();
    private List<StructuralUnitResponseDto> structuralUnitResponseDtos = new ArrayList<>();
}
