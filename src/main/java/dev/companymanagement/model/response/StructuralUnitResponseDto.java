package dev.companymanagement.model.response;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
public class StructuralUnitResponseDto {

    public StructuralUnitResponseDto(Long id, String name, Long parentId){
        this.id = id;
        this.name= name;
        this.parentId = parentId;
    }
    private Long id;
    private String name;
    private Boolean isCompany;
    private Long companyId;
    private Long parentId;
    private List<StructuralUnitResponseDto> children = new ArrayList<>();
}
