package dev.companymanagement.repository;

import dev.companymanagement.domain.StructuralUnit;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface StructuralUnitRepository extends JpaRepository<StructuralUnit, Long> {

    List<StructuralUnit> findByRgtGreaterThanEqual(int nRight);

    List<StructuralUnit> findByLftGreaterThan(int nRight);

    List<StructuralUnit> findAllByOrderByLevelAscLftAsc();

    boolean existsByName(String name);
}
