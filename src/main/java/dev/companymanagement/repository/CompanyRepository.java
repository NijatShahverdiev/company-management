package dev.companymanagement.repository;

import dev.companymanagement.domain.Company;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CompanyRepository extends JpaRepository<Company, Long> {

    boolean existsByName(String name);

    @EntityGraph(attributePaths = "structuralUnits")
    public List<Company> findAllByOrderByLevelAscLftAsc();

    List<Company> findByRgtGreaterThanEqual(int nRight);

    List<Company> findByLftGreaterThan(int nRight);
}
