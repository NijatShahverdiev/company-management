package dev.companymanagement.controller;

import dev.companymanagement.domain.StructuralUnit;
import dev.companymanagement.model.request.StructuralUnitRequestDto;
import dev.companymanagement.model.response.StructuralUnitResponseDto;
import dev.companymanagement.service.structualUnitService.StructuralUnitService;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/structural-units")
@RequiredArgsConstructor
public class StructuralUnitController {
    private final StructuralUnitService structuralUnitService;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public StructuralUnitResponseDto create(@RequestBody @Valid StructuralUnitRequestDto unitRequestDto) {
        return structuralUnitService.create(unitRequestDto);
    }

    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    public List<StructuralUnitResponseDto> structuralUnits() {
        return structuralUnitService.structuralUnits();
    }

    @PutMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void updateStructuralUnit(@PathVariable(name = "id") Long structuralUnitId,
                                     @RequestBody @Valid StructuralUnitRequestDto unitRequestDto) {
        structuralUnitService.update(structuralUnitId, unitRequestDto);
    }
}
