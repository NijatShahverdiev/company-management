package dev.companymanagement.controller;

import dev.companymanagement.model.request.CompanyRequestDto;
import dev.companymanagement.model.response.CompanyResponseDto;
import dev.companymanagement.service.companyService.CompanyService;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@Slf4j
@RequestMapping("/companies")
public class CompanyController {
    private final CompanyService companyService;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public CompanyResponseDto create(@RequestBody @Valid CompanyRequestDto companyRequestDto) {
        return companyService.create(companyRequestDto);
    }

    @GetMapping
    public List<CompanyResponseDto> companies() {
        return companyService.companies();
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable(name = "id") Long companyId) {
        companyService.delete(companyId);
    }
}
