package dev.companymanagement.service.mapper;

import dev.companymanagement.domain.StructuralUnit;
import dev.companymanagement.model.response.StructuralUnitResponseDto;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Component
public class StructuralUnitMapper {

    public List<StructuralUnitResponseDto> mapToStructuralUnitTree(List<StructuralUnit> structuralUnits) {
        Map<Long, StructuralUnitResponseDto> tree = new HashMap<>();
        structuralUnits.forEach(s -> {
            StructuralUnitResponseDto structuralUnitResponseDto =
                    new StructuralUnitResponseDto(s.getId(), s.getName(), s.getParentId());
            structuralUnitResponseDto.setIsCompany(s.getIsCompany());
            structuralUnitResponseDto.setCompanyId(s.getCompany().getId());
            structuralUnitResponseDto.setParentId(s.getParentId());
            tree.put(s.getId(), structuralUnitResponseDto);
        });
        List<Long> rootIds = new ArrayList<>();
        tree.forEach((id, structuralUnitResponseDto) -> {
            if (structuralUnitResponseDto.getParentId() == null) {
                rootIds.add(structuralUnitResponseDto.getId());
                return;
            }
            tree.get(structuralUnitResponseDto.getParentId()).getChildren().add(structuralUnitResponseDto);
        });
        return rootIds
                .stream()
                .map(tree::get)
                .toList();
    }

}
