package dev.companymanagement.service.mapper;

import dev.companymanagement.domain.Company;
import dev.companymanagement.domain.StructuralUnit;
import dev.companymanagement.model.response.CompanyResponseDto;
import dev.companymanagement.model.response.StructuralUnitResponseDto;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Component
@RequiredArgsConstructor
public class CompanyMapper {
    private final StructuralUnitMapper unitMapper;

    public List<CompanyResponseDto> mapToCompanyTree(List<Company> companies) {
        Map<Long, CompanyResponseDto> tree = new HashMap<>();
        companies.forEach(entity -> {
            List<StructuralUnit> structuralUnits = entity.getStructuralUnits().stream().toList();
            List<StructuralUnitResponseDto> structuralUnitResponseDtos =
                    unitMapper.mapToStructuralUnitTree(structuralUnits);
            CompanyResponseDto responseDto = new CompanyResponseDto(entity.getId(), entity.getName());
            responseDto.setIsCompany(entity.getIsCompany());
            responseDto.setParentId(entity.getParentId());
            responseDto.setStructuralUnitResponseDtos(structuralUnitResponseDtos);
            tree.put(entity.getId(), responseDto);


        });
        List<Long> rootIds = new ArrayList<>();
        tree.forEach((id, companyResponseDto) -> {
            if (companyResponseDto.getParentId() == null) {
                rootIds.add(companyResponseDto.getId());
                return;
            }
            tree.get(companyResponseDto.getParentId()).getChildren().add(companyResponseDto);
        });
        return rootIds
                .stream()
                .map(tree::get)
                .toList();
    }
}
