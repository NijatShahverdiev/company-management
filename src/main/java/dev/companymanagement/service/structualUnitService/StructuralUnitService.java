package dev.companymanagement.service.structualUnitService;

import dev.companymanagement.model.request.StructuralUnitRequestDto;
import dev.companymanagement.model.response.StructuralUnitResponseDto;

import java.util.List;

public interface StructuralUnitService {
    StructuralUnitResponseDto create(StructuralUnitRequestDto structuralUnitRequestDto);
    List<StructuralUnitResponseDto> structuralUnits();

    void update(Long id, StructuralUnitRequestDto requestDto);
}
