package dev.companymanagement.service.structualUnitService;

import dev.companymanagement.common.exception.ApplicationException;
import dev.companymanagement.common.exception.Errors;
import dev.companymanagement.domain.StructuralUnit;
import dev.companymanagement.model.request.StructuralUnitRequestDto;
import dev.companymanagement.model.response.StructuralUnitResponseDto;
import dev.companymanagement.repository.StructuralUnitRepository;
import dev.companymanagement.service.companyService.CompanyService;
import dev.companymanagement.service.mapper.StructuralUnitMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service
@RequiredArgsConstructor
public class StructuralUnitServiceImp implements StructuralUnitService {
    private final CompanyService companyService;
    private final StructuralUnitMapper unitMapper;
    private final StructuralUnitRepository unitRepository;

    @Override
    public StructuralUnitResponseDto create(StructuralUnitRequestDto dto) {
        StructuralUnit structuralUnit = new StructuralUnit();
        convertDtoToEntity(structuralUnit, dto);
        StructuralUnit created = unitRepository.save(structuralUnit);
        return new StructuralUnitResponseDto(created.getId(), created.getName(), created.getParentId());
    }

    @Override
    public List<StructuralUnitResponseDto> structuralUnits() {
        List<StructuralUnit> structuralUnits = unitRepository.findAllByOrderByLevelAscLftAsc();
        return unitMapper.mapToStructuralUnitTree(structuralUnits);
    }

    @Override
    public void update(Long id, StructuralUnitRequestDto requestDto) {
        StructuralUnit structuralUnit = findById(id);
        convertDtoToEntity(structuralUnit, requestDto);
        unitRepository.save(structuralUnit);
    }

    public void convertDtoToEntity(StructuralUnit structuralUnit, StructuralUnitRequestDto dto) {
        checkIfStructuralUnitExistByName(dto.getName());
        structuralUnit.setName(dto.getName());
        if (dto.getParentId() != null && dto.getCompanyId() != null)
            throw new ApplicationException(Errors.MULTIPLE_PARENT_SELECTION);
        if (dto.getParentId() != null) {
            StructuralUnit parent = findById(dto.getParentId());
            structuralUnit.setLevel(parent.getLevel() + 1);
            structuralUnit.setLft(parent.getRgt());
            structuralUnit.setRgt(parent.getRgt() + 1);
            structuralUnit.setParentId(dto.getParentId());
            structuralUnit.setCompany(null);
            setAllRightGreatOrEqualsParentRight(parent.getRgt());
            setAllLeftGreaterThanParentRight(parent.getRgt());
        } else if (dto.getCompanyId() != null) {
            companyService.checkCompanyWithTheGivenId(dto.getCompanyId());
            structuralUnit.getCompany().setId(dto.getCompanyId());
            structuralUnit.setLevel(1);
            structuralUnit.setLft(0);
            structuralUnit.setRgt(1);
        }
    }

    private void setAllRightGreatOrEqualsParentRight(int nRight) {
        unitRepository.findByRgtGreaterThanEqual(nRight)
                .forEach(u -> u.setRgt(u.getRgt() + 2));
    }

    private void setAllLeftGreaterThanParentRight(int nRight) {
        unitRepository.findByLftGreaterThan(nRight)
                .forEach(u -> u.setLft(u.getLft() + 2));
    }

    private void checkIfStructuralUnitExistByName(String name) {
        if (unitRepository.existsByName(name))
            throw new ApplicationException(Errors.STRUCTURAL_UNIT_ALREADY_EXIST, Map.of("name", name));
    }

    private StructuralUnit findById(Long id) {
        return unitRepository.findById(id)
                .orElseThrow(() -> new ApplicationException(Errors.STRUCTURAL_UNIT_NOT_FOUND, Map.of("id", id)));
    }

}
