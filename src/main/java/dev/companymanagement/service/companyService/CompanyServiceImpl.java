package dev.companymanagement.service.companyService;

import dev.companymanagement.common.exception.ApplicationException;
import dev.companymanagement.common.exception.Errors;
import dev.companymanagement.domain.Company;
import dev.companymanagement.model.request.CompanyRequestDto;
import dev.companymanagement.model.response.CompanyResponseDto;
import dev.companymanagement.model.response.StructuralUnitResponseDto;
import dev.companymanagement.repository.CompanyRepository;
import dev.companymanagement.service.mapper.CompanyMapper;
import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
@RequiredArgsConstructor
@Slf4j
public class CompanyServiceImpl implements CompanyService {
    private final CompanyRepository companyRepository;
    private final CompanyMapper companyMapper;

    @Override
    public CompanyResponseDto create(CompanyRequestDto dto) {
        checkIfCompanyExistsWithTheGivenName(dto);
        Company company = new Company();
        company.setName(dto.getName());
        if (dto.getParentCompanyId() != null) {
            Company parent = findById(dto.getParentCompanyId());
            company.setLft(parent.getRgt());
            company.setRgt(parent.getRgt() + 1);
            company.setLevel(parent.getLevel() + 1);
            company.setParentId(parent.getId());
            setAllRightGreatOrEqualsParentRight(parent.getRgt());
            setAllLeftGreaterThanParentRight(parent.getRgt());
        } else {
            company.setLft(0);
            company.setRgt(1);
            company.setLevel(1);
        }
        Company created = companyRepository.save(company);
        return new CompanyResponseDto(created.getId(), created.getName());
    }

    @Transactional
    @Override
    public List<CompanyResponseDto> companies() {
        List<Company> companies = companyRepository.findAllByOrderByLevelAscLftAsc();
        return companyMapper.mapToCompanyTree(companies);
    }

    @Override
    public void delete(Long companyId) {
        Company company = findById(companyId);
        if (company.getRgt() > 1)
            throw new ApplicationException(Errors.COMPANY_HAS_CHILDREN, Map.of("id", companyId));
        companyRepository.delete(company);
    }

    private void setAllRightGreatOrEqualsParentRight(int nRight) {
        companyRepository.findByRgtGreaterThanEqual(nRight)
                .forEach(c -> c.setRgt(c.getRgt() + 2));
    }

    private void setAllLeftGreaterThanParentRight(int nRight) {
        companyRepository.findByLftGreaterThan(nRight)
                .forEach(c -> c.setLft(c.getLft() + 2));
    }

    private void checkIfCompanyExistsWithTheGivenName(CompanyRequestDto companyRequestDto) {
        if (companyRepository.existsByName(companyRequestDto.getName()))
            throw new ApplicationException(Errors.COMPANY_ALREADY_EXIST,
                    Map.of("name", companyRequestDto.getName()));
    }

    @Override
    public Company findById(Long companyId) {
        return companyRepository.findById(companyId)
                .orElseThrow(() -> new ApplicationException(Errors.COMPANY_NOT_FOUND, Map.of("id", companyId)));
    }

    @Override
    public void checkCompanyWithTheGivenId(Long id) {
        if (!companyRepository.existsById(id))
            throw new ApplicationException(Errors.COMPANY_NOT_FOUND, Map.of("id", id));
    }
}
