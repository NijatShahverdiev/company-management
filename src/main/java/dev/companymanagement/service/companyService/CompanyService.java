package dev.companymanagement.service.companyService;


import dev.companymanagement.domain.Company;
import dev.companymanagement.model.request.CompanyRequestDto;
import dev.companymanagement.model.response.CompanyResponseDto;

import java.util.List;

public interface CompanyService {
    CompanyResponseDto create(CompanyRequestDto companyRequestDto);

    List<CompanyResponseDto> companies();

    Company findById(Long id);
    void checkCompanyWithTheGivenId(Long id);
    void delete(Long companyId);
}
