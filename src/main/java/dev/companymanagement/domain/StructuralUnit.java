package dev.companymanagement.domain;

import jakarta.persistence.*;
import lombok.Data;

@Entity
@Data
public class StructuralUnit {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String name;
    private Integer lft;
    private Integer rgt;
    private Integer level;
    private Long parentId;
    @Column(columnDefinition = "TINYINT(1) default 0")
    private Boolean isCompany = false;
    @ManyToOne(fetch = FetchType.LAZY)
    private Company company = new Company();
}
