package dev.companymanagement.domain;

import com.fasterxml.jackson.annotation.JsonProperty;
import jakarta.persistence.*;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import java.util.HashSet;
import java.util.Set;

@Data
@Entity
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
@ToString(exclude = "structuralUnits")
public class Company {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String name;
    private Integer lft;
    private Integer rgt;
    private Integer level;
    private Long parentId;
    @Column(columnDefinition = "TINYINT(1) default 1")
    private Boolean isCompany = true;
    @OneToMany(mappedBy = "company", cascade = CascadeType.ALL, orphanRemoval = true)
    private Set<StructuralUnit> structuralUnits = new HashSet<>();

}
