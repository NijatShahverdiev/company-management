package dev.companymanagement.common.config;

public class HttpConstants {
    public static final String ACCEPT_HEADER = "accept";

    public static final String CONTENT_TYPE = "Content-Type";

    private HttpConstants() {
        throw new UnsupportedOperationException();
    }
}
