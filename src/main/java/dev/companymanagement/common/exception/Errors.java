package dev.companymanagement.common.exception;

import org.springframework.http.HttpStatus;

public enum Errors implements ErrorResponse {
    COMPANY_HAS_CHILDREN("COMPANY_HAS_CHILDREN", HttpStatus.BAD_REQUEST,
            "The company with the given id {id} has children"),
    COMPANY_ALREADY_EXIST("COMPANY_ALREADY_EXIST", HttpStatus.BAD_REQUEST,
            "The company with the given name {name} already exist"),
    COMPANY_NOT_FOUND("COMPANY_NOT_FOUND", HttpStatus.NOT_FOUND,
            "The company not found with the given id: {id}"),
    MULTIPLE_PARENT_SELECTION("MULTIPLE_PARENT_SELECTION", HttpStatus.NOT_FOUND,
            "A structural unit can has only a parent company or structural unit"),
    STRUCTURAL_UNIT_ALREADY_EXIST("STRUCTURAL_UNIT_ALREADY_EXIST", HttpStatus.NOT_FOUND,
            "Structural unit with the given name: {name} already exists"),
    STRUCTURAL_UNIT_NOT_FOUND("STRUCTURAL_UNIT_NOT_FOUND", HttpStatus.NOT_FOUND,
            "Structural unit with the given id {id} not found");


    final String key;
    final HttpStatus httpStatus;
    final String message;

    Errors(String key, HttpStatus httpStatus, String message) {
        this.message = message;
        this.key = key;
        this.httpStatus = httpStatus;
    }

    @Override
    public String getKey() {
        return key;
    }

    @Override
    public String getMessage() {
        return message;
    }

    @Override
    public HttpStatus getHttpStatus() {
        return httpStatus;
    }

}
